package com.chongba.schedule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chongba.schedule.pojo.TaskInfoLogsEntity;


public interface TaskInfoLogsMapper extends BaseMapper<TaskInfoLogsEntity> {
}
